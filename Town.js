/**
 *	@filename	Town.js (modded)
 *	@author		spaceleak
 *	@desc		manage inventory, belt, stash, cube
 */

// arrange inventory items so we have room
Town.invSort = function() {
    let fit = { sizex: 4, sizey: 4 };

    if (!Storage.Inventory.CanFit(fit)) {
        me.cancel();

        return me.findItems(-1, -1, 3)
            .sort((a, b) => b.sizex * b.sizey - a.sizex * a.sizey)
            .forEach(i => Storage.Inventory.MoveTo(me.itemoncursor ? getUnit(101) : i, true));
    }

    return true;
};

// Lower gold requirement
// buy potions in act 3 in normal instead of act 4 - if gold low
Town.buyPotions = function () {
    if ((me.getStat(14) + me.getStat(15)) < (me.diff === 0 ? 100 : 1000)) { // Ain't got money fo' dat shyt
        return false;
    }   

    var i, j, npc, useShift, col, beltSize, pot,
        needPots = false,
        needBuffer = true,
        buffer = {
            hp: 0,
            mp: 0
        };

    let preAct;

    beltSize = Storage.BeltSize();
    col = Town.checkColumns(beltSize);

    // HP/MP Buffer
    if (Config.HPBuffer > 0 || Config.MPBuffer > 0) {
        pot = me.getItem(-1, 0);

        if (pot) {
            do {
                if (pot.location === 3) {
                    switch (pot.itemType) {
                    case 76:
                        buffer.hp += 1;

                        break;
                    case 77:
                        buffer.mp += 1;

                        break;
                    }
                }
            } while (pot.getNext());
        }
    }

    // Check if we need to buy potions based on Config.MinColumn
    for (i = 0; i < 4; i += 1) {
        if (["hp", "mp"].indexOf(Config.BeltColumn[i]) > -1 && col[i] > (beltSize - Math.min(Config.MinColumn[i], beltSize))) {
            needPots = true;
        }
    }

    // Check if we need any potions for buffers
    if (buffer.mp < Config.MPBuffer || buffer.hp < Config.HPBuffer) {
        for (i = 0; i < 4; i += 1) {
            // We can't buy potions because they would go into belt instead
            if (col[i] >= beltSize && (!needPots || Config.BeltColumn[i] === "rv")) {
                needBuffer = false;

                break;
            }
        }
    }

    // We have enough potions in inventory
    if (buffer.mp >= Config.MPBuffer && buffer.hp >= Config.HPBuffer) {
        needBuffer = false;
    }

    // No columns to fill
    if (!needPots && !needBuffer) {
        return true;
    }

    // save gold and buy cheaper potions
    if (me.diff == 0 && me.act >= 4 && me.gold < 120000) {
        preAct = me.act;

        Town.goToTown(3);
    }

    npc = Town.initNPC("Shop", "buyPotions");

    if (!npc) {
        if (preAct) {
            Town.goToTown(preAct);
        }

        return false;
    }

    for (i = 0; i < 4; i += 1) {
        if (col[i] > 0) {
            useShift = Town.shiftCheck(col, beltSize);
            pot = Town.getPotion(npc, Config.BeltColumn[i]);

            if (pot) {
                //print("ÿc2column ÿc0" + i + "ÿc2 needs ÿc0" + col[i] + " ÿc2potions");

                // Shift+buy will trigger if there's no empty columns or if only the current column is empty
                if (useShift) {
                    pot.buy(true);
                } else {
                    for (j = 0; j < col[i]; j += 1) {
                        pot.buy(false);
                    }
                }
            }
        }

        col = Town.checkColumns(beltSize); // Re-initialize columns (needed because 1 shift-buy can fill multiple columns)
    }

    if (needBuffer && buffer.hp < Config.HPBuffer) {
        for (i = 0; i < Config.HPBuffer - buffer.hp; i += 1) {
            pot = Town.getPotion(npc, "hp");

            if (Storage.Inventory.CanFit(pot)) {
                pot.buy(false);
            }
        }
    }

    if (needBuffer && buffer.mp < Config.MPBuffer) {
        for (i = 0; i < Config.MPBuffer - buffer.mp; i += 1) {
            pot = Town.getPotion(npc, "mp");

            if (Storage.Inventory.CanFit(pot)) {
                pot.buy(false);
            }
        }
    }

    if (preAct) {
        Town.goToTown(preAct);
    }

    return true;
};

// Don't ignore ID tome
Town.checkScrolls = function (id) {
    var tome = me.findItem(id, 0, 3);

    return tome ? tome.getStat(70) : 0;
};

// Also buy id tome
// Increase gold
Town.fillTome = function (code) {
    if (me.gold < 2000) {
        return false;
    }

    if (this.checkScrolls(code) >= 9) {
        return true;
    }

    var scroll, tome,
        npc = this.initNPC("Shop", "fillTome");

    if (!npc) {
        return false;
    }

    delay(500);

    if (!me.findItem(code, 0, 3)) {
        tome = npc.getItem(code);

        if (tome && Storage.Inventory.CanFit(tome)) {
            try {
                tome.buy();
            } catch (e1) {
                print(e1);

                // Couldn't buy the tome, don't spam the scrolls
                return false;
            }
        } else {
            return false;
        }
    }

    scroll = npc.getItem(code === 518 ? 529 : 530);

    if (!scroll) {
        return false;
    }

    try {
        scroll.buy(true);
    } catch (e2) {
        print(e2.message);

        return false;
    }

    return true;
};

// Use packets
Town.reviveMerc = function () {
    if (!Town.needMerc()) {
        return true;
    }

    // Fuck Aheara
    if (me.act === 3) {
        Town.goToTown(2);
    }

    var preArea = me.area,
        npc = Town.initNPC("Merc", "reviveMerc");

    if (!npc) {
        return false;
    }

    npc = getInteractedNPC();

    if (!npc) {
        return false;
    }

ReviveLoop:
    for (let i = 0; i < 15; i += 1) {
        sendPacket(1, 0x62, 4, npc.gid);

        let tick = getTickCount();

        while (getTickCount() - tick < Math.max(1000, me.ping * 2 + 200)) {
            if (me.getMerc()) {
                me.cancel(); // fix crash

                break ReviveLoop;
            }

            delay(10);
        }
    }

    me.cancel();

    Attack.checkInfinity();

    if (!!me.getMerc()) {
        if (Config.MercWatch) { // Cast BO on merc so he doesn't just die again
            print("MercWatch precast");
            Pather.useWaypoint("random");
            Precast.doPrecast(true);
            Pather.useWaypoint(preArea);
        }

        return true;
    }

    return false;
};

// added quests
// use packets
Town.checkQuestItems = function () {
    // skill book
    if (me.getItem(552)) {
        Quest.useItem(552);
    }

    // golden bird stuff
    if (me.getItem(546)) {
        Town.goToTown(3);
        Packet.questMessage("meshif", "meshif", 529);
    }

    if (me.getItem(547)) {
        Town.goToTown(3);
        Packet.questMessage("stash", "alkor", 534);
        Packet.questMessage("stash", "alkor", 538);
    }

    if (me.getItem(545)) {
        Quest.useItem(545);
    }

    // anya resist scroll
    if (me.getItem(646)) {
        Quest.useItem(646);
    }

    return true;
};

// we can end up with junk items in stash forever
// when gem cubing is changed and we still have old gems the gems will never get removed
// called when we stash gold
Town.clearStash = function () {
    return me.findItems(-1, -1, 7).forEach(function(item) {
        if ((Pickit.checkItem(item).result == 4 || NTIP.CheckItem(item, NTIP_CheckListNoTier, true).result == 0) &&
            (!Cubing.keepItem(item) && !Runewords.keepItem(item) && !CraftingSystem.keepItem(item))) {
            Packet.dropItem(item);
        }
    });
};