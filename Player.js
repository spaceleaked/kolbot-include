/**
 *	@filename	Player.js
 *	@author		spaceleak
 *	@desc		functions related to getting player infos such as area, ingame
 */

var Player = (function() {
    return {
        count: {
            inGame: function() {
                let count = 0;
                let party = getParty();

                if (party) {
                    do {
                        count += 1;
                    } while (party.getNext());
                }

                return count;
            },
            inParty: function() {
                let count = 0;
                let party = getParty();

                if (party) {
                    let myPartyId = party.partyid;

                    do {
                        if (party.partyid !== 65535 && party.partyid === myPartyId) {
                            count += 1;
                        }
                    } while (party.getNext());
                }

                return count;
            },
            inArea: function(area=me.area) {
                let count = 0;
	            let party = getParty();

	            if (party) {
	                do {
	                    if (party.area == area) {
	                        count += 1;
	                    }
	                } while (party.getNext());
	            }

                // we don't get our own area from the party object
                if (me.area == area) {
                    count += 1;
                }

	            return count;
	        },
            nearby: function() {
                let count = 0;
                let player = getUnit(0);

                if (player) {
                    do {
                        if (player.name != me.name) {
                            count += 1;
                        }
                    } while (player.getNext());
                }

                return count;
            }
        },
        wait: {
            forState: function(state, duration=60000) {
                let tick = getTickCount();

                while (getTickCount() - tick < duration) {
                    if (me.getState(state)){
                        return true;
                    }

                    delay(50);
                }

                return false;
            },
            toJoin: function(name, duration=120000) {
	            let tick = getTickCount();

	            while (getTickCount() - tick < duration) {
	                if (getParty(name)){
	                    return true;
	                }

	                delay(50);
	            }

	            return false;
	        },
            inGame: function(amount, duration=90000) {
                let tick = getTickCount();

                while (getTickCount() - tick < duration) {
                    if (Player.count.inGame() >= amount){
                        return true;
                    }

                    delay(50);
                }

                return false;
            },
            inArea: function(area=me.area, amount=1, duration=90000) {
                let tick = getTickCount();

                while (getTickCount() - tick < duration) {
                    if (Player.count.inArea(area) >= amount){
                        return true;
                    }

                    delay(50);
                }

                return false;
            },
            nearby: function(amount=0, duration=30000) {
                let tick = getTickCount();

                while (getTickCount() - tick < duration) {
                    if (Player.count.nearby() == amount){
                        return true;
                    }

                    delay(50);
                }

                return false;
            }
        }
    }
}());
