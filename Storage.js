/**
 *	@filename	Storage.js (modded)
 *	@author		spaceleak
 *	@desc		manage inventory, belt, stash, cube
 */

// Use packets to move item
// have a sorting parameter, location will already be 3 so we bypass that check if sorting
Storage.Inventory.MoveTo = function(item, sorting) {
    if (item.mode === 3) {
        return false;
    }

    if (item.location === 3 && !sorting) {
        return true;
    }

    let spot = Storage.Inventory.FindSpot(item);

    if (!spot) {
        return false;
    }

    if (Packet.itemToCursor(item)) {
        for (let i = 0; i < 15; i += 1) {
            // 18 [DWORD id] [DWORD xpos] [DWORD ypos] [DWORD buffer]
            sendPacket(1, 0x18, 4, item.gid, 4, spot.y, 4, spot.x, 4, 0x00);

            let tick = getTickCount();

            while (getTickCount() - tick < Math.max(1000, me.ping * 2 + 200)) {
                if (!me.itemoncursor) {
                    return true;
                }

                delay(10);
            }
        }
    }

    return false;
};

// Use packets to move item
Storage.Stash.MoveTo = function(item) {
    if (item.mode === 3) {
        return false;
    }

    if (item.location === 7) {
        return true;
    }

    let spot = Storage.Stash.FindSpot(item);

    if (!spot) {
        return false;
    }

    if (Packet.itemToCursor(item)) {
        for (let i = 0; i < 15; i += 1) {
            // 18 [DWORD id] [DWORD xpos] [DWORD ypos] [DWORD buffer]
            sendPacket(1, 0x18, 4, item.gid, 4, spot.y, 4, spot.x, 4, 0x04);

            let tick = getTickCount();

            while (getTickCount() - tick < Math.max(1000, me.ping * 2 + 200)) {
                if (!me.itemoncursor) {
                    return true;
                }

                delay(10);
            }
        }
    }

    return false;
};

// Use packets to move item
Storage.Cube.MoveTo = function(item) {
    if (item.mode === 3) {
        return false;
    }

    if (item.location === 6) {
        return true;
    }

    let spot = Storage.Cube.FindSpot(item);

    if (!spot) {
        return false;
    }

    if (Packet.itemToCursor(item)) {
        for (let i = 0; i < 15; i += 1) {
            // 18 [DWORD id] [DWORD xpos] [DWORD ypos] [DWORD buffer]
            sendPacket(1, 0x18, 4, item.gid, 4, spot.y, 4, spot.x, 4, 0x03);

            let tick = getTickCount();

            while (getTickCount() - tick < Math.max(1000, me.ping * 2 + 200)) {
                if (!me.itemoncursor) {
                    return true;
                }

                delay(10);
            }
        }
    }

    return false;
};

// Access cube anywhere
Storage.accessCube = function() {
    let cube = me.getItem(549);

    if (!cube) {
        return false;
    }

    if (cube.location === 3) {
        for (let i = 0; i < 7; i += 1) {
            cube.interact();

            let tick = getTickCount();

            while (getTickCount() - tick < 1000) {
                if (getUIFlag(0x1A)) {
                    delay(200 + me.ping);

                    return true;
                }

                delay(10);
            }
        }
    } else if (cube.location === 7) {
        let item = me.findItem(-1, -1);

        if (item) {
            do {
                if (item.classid !== 549 && [1, 3, 7].indexOf(item.location) > -1) {
                    break;
                }
            } while (item.getNext());
        }

        if (item) {
            for (let i = 0; i < 7; i += 1) {
                sendPacket(1, 0x27, 4, item.gid, 4, cube.gid); //access cube

                let tick = getTickCount();

                while (getTickCount() - tick < 1000) {
                    if (getUIFlag(0x1A)) {
                        delay(200 + me.ping);

                        return true;
                    }

                    delay(10);
                }
            }
        }
    }

    return false;
};


