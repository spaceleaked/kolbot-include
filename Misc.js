/**
 *	@filename	Misc.js (modded)
 *	@author		spaceleak
 *	@desc		misc library containing Skill, Misc and Sort classes
 */

Misc.charClass = ["Amazon", "Sorceress", "Necromancer", "Paladin", "Barbarian", "Druid", "Assassin"];
Misc.difficultyString = ["Normal", "Nightmare", "Hell"];

// updates config obj across all threads
Misc.updateConfig = function() {
    scriptBroadcast("config--" + JSON.stringify(Misc.copy(Config)));
};

Misc.randomString = function(len, num) {
    let possible = 'abcdefghijklmnopqrstuvwxyz';

    if (num) {
        possible += '0123456789';
    }

    let text = '';

    for (let i = 0; i < len; i += 1) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }

    return text;
};

Misc.logCoords = function(path="libs/sonic/logs/coord.json") {
    if (!FileTools.exists(path)) {
        let list = { coords: [] };

        Misc.dataAction.create(path, list, 0);
    }

    print('Logging x: ' + me.x + ', y: ' + me.y);

    return Misc.dataAction.update(path, "coords", { x: me.x, y: me.y, radius: 10 }, 0);
};

// perform CRUD operations on data files
Misc.dataAction = {
    create: function(path, obj, format=2) {
        return Misc.fileAction(path, 1, JSON.stringify(obj, null, format));
    },
    read: function(path) {
        return JSON.parse(Misc.fileAction(path, 0));
    },
    update: function(path, prop, value, format=2) {
        let obj = this.read(path);

        if (obj.hasOwnProperty(prop) && Array.isArray(obj[prop])) {
            if (obj[prop].includes(value)) {
                return true;
            } else {
                obj[prop].push(value);
            }
        } else {
            obj[prop] = value;
        }

        return Misc.fileAction(path, 1, JSON.stringify(obj, null, format));
    },
    delete: function(path, prop) {

    }
};

// add a flash - increase retry amount
Misc.openChest = function(unit) {
    // Skip invalid and Countess chests
    if (!unit || unit.x === 12526 || unit.x === 12565) {
        return false;
    }

    // already open
    if (unit.mode) {
        return true;
    }

    // locked chest, no keys
    if (me.classid !== 6 && unit.islocked && !me.findItem(543, 0, 3)) {
        return false;
    }

    var i, tick;

    for (i = 0; i < 5; i += 1) {
        if (Pather.moveTo(unit.x + 1, unit.y + 2, 3) && getDistance(me, unit.x + 1, unit.y + 2) < 5) {
            //Misc.click(0, 0, unit);
            sendPacket(1, 0x13, 4, unit.type, 4, unit.gid);
        }

        tick = getTickCount();

        while (getTickCount() - tick < 1000) {
            if (unit.mode) {
                return true;
            }

            delay(10);
        }

        Packet.flash(me.gid);
    }

    if (!me.idle) {
        Misc.click(0, 0, me.x, me.y); // Click to stop walking in case we got stuck
    }

    return false;
};

Packet.entityAction = function(npc) {
    let entity = getUnit(1, npc);

    while (!entity) {
        Town.move(npc);
        Packet.flash(me.gid);
        delay(me.ping * 2);

        entity = getUnit(1, npc);
    }

    sendPacket(1, 0x38, 4, 0, 4, entity.gid, 4, 0);

    return true;
};

Packet.questMessage = function(where, who, message) {
    let npc = getUnit(1, who);

    while (!npc) {
        Town.move(where);
        Packet.flash(me.gid);
        delay(me.ping * 2);

        npc = getUnit(1, who);
    }

    if (npc) {
        let tick = getTickCount();

        while (getTickCount() - tick < 1000) {
            sendPacket(1, 0x31, 4, npc.gid, 4, message);
            delay(me.ping);
        }

        return true;
    }

    return false;
};

Packet.checkQuest = (id, state, time=5) => {
    let tick = getTickCount();

    while (getTickCount() - tick < time) {
        sendPacket(1, 0x40);

        if (me.getQuest(id, state)) {
            return true;
        }

        delay(5);
    }

    return false;
};